from django.utils.translation import ugettext_noop
from courseware.tabs import CourseTab
from django.conf import settings


class AdobeConnect(CourseTab):
    """
    The representation of the course teams view type.
    """

    type = "adobe_connect"
    name = "adobe_connect"
    title = ugettext_noop("Aula Virtual")
    view_name = "django_logic_view"
    is_default = True
    tab_id = "adobe_connect_id"
    is_hideable = True


    @classmethod
    def is_enabled(cls, course, user=None):
        return settings.FEATURES.get('IS_ADOBE_CONNECT_TAB_ENABLED', False)
