from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required

from .views import adobe_connect

urlpatterns = patterns(
    'adobe_connect.views',
    url(r"^/$", login_required(adobe_connect), name="django_logic_view")
)
