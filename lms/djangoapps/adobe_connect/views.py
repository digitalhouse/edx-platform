from django.shortcuts import render_to_response
from opaque_keys.edx.keys import CourseKey
from courseware.courses import get_course_with_access


def adobe_connect(request, course_id):
    course_key = CourseKey.from_string(course_id)
    course = get_course_with_access(request.user, "load", course_key)

    context = {
        "course": course,
        "course_key": course_key,
    }
    return render_to_response("adobe_connect/adobe_connect.html", context)
